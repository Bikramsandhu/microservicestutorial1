﻿using EventBus.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventBus.Abstractions
{
	public interface IEventBus
	{
		void Publish(IntegrationEvent @event);

		void Subscribe<T, Th>()
			where T : IntegrationEvent
			where Th : IIntegrationEventHandler<T>;

		void SubscribeDynamic<TH>(string eventName)
			where TH : IDynamicIntegrationEventHandler;

		void UnsubscribeDynamic<TH>(string eventName)
			where TH : IDynamicIntegrationEventHandler;

		void Unsubscribe<T, TH>()
			where TH : IIntegrationEventHandler<T>
			where T : IntegrationEvent;
	}
}

