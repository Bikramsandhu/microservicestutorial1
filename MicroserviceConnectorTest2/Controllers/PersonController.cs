﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroserviceConnectorTest1.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MicroserviceConnectorTest2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private PersonContext _context { get; set; }

        public PersonController(PersonContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult<Person>> PersonPost(Person person)
        {
            try
            {
                var person1 = _context.person.Add(person);
                await _context.SaveChangesAsync();
                return Content("Person has been posted");
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }
    }
}