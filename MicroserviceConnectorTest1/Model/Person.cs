﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroserviceConnectorTest1.Model
{
    public class Person
    {
        public int PersonID { get; set; }
        public string Name { get; set; }
    }
}
