﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroserviceConnectorTest1.Model
{
    public class PersonContext : DbContext
    {
        public DbSet<Person> person { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=personMicroservice;Username=postgres;Password=Bikramsandhu1");
        public PersonContext() 
        {

        }
    }
}
