﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroserviceConnectorTest1.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MicroserviceConnectorTest1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private PersonContext _context;

        public PersonController(PersonContext context)
        {

        }

        [HttpPost]
        public async Task<ActionResult<Person>> PersonPost(Person person)
        {
            try
            {
                var postPerson = _context.person.Add(person);
                await _context.SaveChangesAsync();
                return Content("Person is saved into db.");
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
          


        }
    }
}