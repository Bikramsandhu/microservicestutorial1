﻿using Autofac;
using EventBus.Abstractions;
using EventBus.Events;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace EventBusRabbitMQ
{
	public class EventBusRabbitMQ : IEventBus, IDisposable
	{
		const string BROKER_NAME = "rabbitmq_event_bus";

		private readonly IRabbitMQPersistentConnection _persistentConnection;
		private readonly ILogger<EventBusRabbitMQ> _logger;
		private readonly int _retryCount;

		private IModel _consumerChannel;
		private string _queueName;

		public EventBusRabbitMQ(IRabbitMQPersistentConnection persistentConnection, ILogger<EventBusRabbitMQ> logger,
			string queueName = null, int retryCount = 5)
		{
			_persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_queueName = queueName;
			_retryCount = retryCount;
			_consumerChannel = CreateConsumerChannel();
		}

		public void Publish(IntegrationEvent @event)
		{
			if (!_persistentConnection.IsConnected)
			{
				_persistentConnection.TryConnect();
			}

			var policy = RetryPolicy.Handle<BrokerUnreachableException>()
				.Or<SocketException>()
				.WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
				{
					_logger.LogWarning(ex, "Could not publish event: {EventId} after {Timeout}s ({ExceptionMessage})", @event.Id, $"{time.TotalSeconds:n1}",ex.Message);
				});

			var eventName = @event.GetType().Name;

			_logger.LogTrace("Creating RabbitMQ channel is publish event: {EventId} ({EventName})", @event.Id, eventName);
			using(var channel = _persistentConnection.CreateModel())
			{
				_logger.LogTrace("Declaring RabbitMQ exchange to publish event: {EventId}", @event.Id);

				channel.ExchangeDeclare(exchange: BROKER_NAME, type: "direct");

				var message = JsonConvert.SerializeObject(@event);
				var body = Encoding.UTF8.GetBytes(message);

				policy.Execute(() =>
				{
					var properties = channel.CreateBasicProperties();
					properties.DeliveryMode = 2;

					_logger.LogTrace("Publishing event to RabbitMQ: {EventId}", @event.Id);

					channel.BasicPublish(
						exchange: BROKER_NAME,
						routingKey: eventName,
						mandatory:true, 
						basicProperties:properties,
						body:body);
				});
			}
		}

		public void Subscribe<T, Th>()
			where T : IntegrationEvent
			where Th : IIntegrationEventHandler<T>
		{
			throw new NotImplementedException();
		}

		public void SubscribeDynamic<TH>(string eventName) 
			where TH : IDynamicIntegrationEventHandler
		{
			//_logger.LogInformation("Subscribing to dynamic event {EventName} with {EventHandler}", eventName, typeof(TH).GetGenericTypeName());
		}

		public void Unsubscribe<T, TH>()
			where T : IntegrationEvent
			where TH : IIntegrationEventHandler<T>
		{
			throw new NotImplementedException();
		}

		public void UnsubscribeDynamic<TH>(string eventName) where TH : IDynamicIntegrationEventHandler
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			if(_consumerChannel != null)
			{
				_consumerChannel.Dispose();
			}
		}

		private IModel CreateConsumerChannel()
		{
			if (!_persistentConnection.IsConnected)
			{
				_persistentConnection.TryConnect();
			}

			_logger.LogTrace("Creating RabbitMQ consumer channel");

			var channel = _persistentConnection.CreateModel();

			channel.ExchangeDeclare(exchange:BROKER_NAME,
				type:"direct");

			channel.QueueDeclare(queue:_queueName,
				durable:true,
				exclusive:false,
				autoDelete:false,
				arguments:null);

			channel.CallbackException += (sender, ea) =>
			{
				_logger.LogWarning(ea.Exception, "Recreating RabbitMQ consumer channel");

				_consumerChannel.Dispose();
				_consumerChannel = CreateConsumerChannel();
				StartBasicConsumer();
			};

			return channel;
		}

		private void StartBasicConsumer()
		{
			_logger.LogTrace("Starting RabbitMQ basic consumer");

			if(_consumerChannel != null)
			{
				var consumer = new AsyncEventingBasicConsumer(_consumerChannel);

				consumer.Received += Consumer_Received;
				_consumerChannel.BasicConsume(
					queue:_queueName,
					autoAck: false,
					consumer:consumer);
			}
			else
			{
				_logger.LogError("StartBasicConsume can't call on _consumerChannel == null");
			}
		}

		private async Task Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
		{
			var eventName = eventArgs.RoutingKey;
			var message = Encoding.UTF8.GetString(eventArgs.Body);

			try
			{
				if (message.ToLowerInvariant().Contains("throw-fake-exception"))
				{
					throw new InvalidOperationException($"Fake exception requested: \"{message}\"");
				}

				//await ProcessEvent(eventName,message);
			}catch(Exception ex)
			{
			   _logger.LogWarning(ex, "---Error Processing message \"{Message}\"", message);
			}
			_consumerChannel.BasicAck(eventArgs.DeliveryTag, multiple:false);
		}

		
	}
}
